# Random beacon

This repository contains reference implementation of keygen, tiers 1, 2 and 3 from paper 
"Mt. Random: Multi-Tiered Randomness Beacons". We simulated several parties carrying out 
the protocol and measured performance, raw performance reports can be found [here](./reports/performance).

Experiments can be reproduced locally by running tests (that will print reports to stdout):

```bash
HEAVY_TESTS=1 cargo test --release -- --nocapture
```

However, it would take a lot of time, so it's better to run each test individually. E.g. if you
want to run performance tests for tier1, run this:

```bash
HEAVY_TESTS=1 cargo test --release tier1::tests::tier1_protocol_performance_with_no_adversaries -- --exact --nocapture
```

You may find other advanced performance tests that, for instance, measure performance in presence
of adversaries by looking through `tests.rs` files: performance tests contain `performance` word
in name of the test, and require `HEAVY_TESTS` env variable to be set.
